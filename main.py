from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/version")
def get_version():
    return {
        "version": "demo-1",
        "project": "ArgoCD - Demo",
        "key": "Demo screencast",
        "demo": "test",
        }
